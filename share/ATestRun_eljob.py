#!/usr/bin/env python

# Read the submission directory as a command line argument. You can
# extend the list of arguments with your private ones later on.
import argparse
parser = argparse.ArgumentParser()
parser.add_argument('-s', '--submitDir', default='submitDir',
                    help = 'Submission directory for EventLoop' )
args = parser.parse_args()

# Set up (Py)ROOT.
import ROOT
ROOT.xAOD.Init().ignore()

# Set up the sample handler object. See comments from the C++ macro
# for the details about these lines.
import os
sh = ROOT.SH.SampleHandler()
sh.setMetaString( 'nc_tree', 'CollectionTree' )
inputFilePath = os.getenv('CFS')+'/atlas/kkrizka/lowmu/run'
ROOT.SH.ScanDir().filePattern( 'DAOD_IDTRKVALID_1.pool.root' ).scan( sh, inputFilePath )
sh.printContent()

# Create an EventLoop job.
job = ROOT.EL.Job()
job.sampleHandler( sh )
job.options().setDouble( ROOT.EL.Job.optMaxEvents, 500 )
job.options().setString( ROOT.EL.Job.optSubmitDirMode, 'unique-link')

# Create the algorithm's configuration.
from AnaAlgorithm.DualUseConfig import createAlgorithm
alg = createAlgorithm ( 'TruthParticleDumper', 'TruthParticles' )
alg.CollectionName = "TruthParticles"

# later on we'll add some configuration options for our algorithm that go here

# Add our algorithm to the job
job.algsAdd( alg )

# Add output directory to job
job.outputAdd(ROOT.EL.OutputStream('PARQUET'))

# Run the job using the direct driver.
driver = ROOT.EL.DirectDriver()
driver.submit( job, args.submitDir )
