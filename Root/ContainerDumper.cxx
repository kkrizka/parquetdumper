#include <ParquetDumper/ContainerDumper.h>

#include <EventLoop/Worker.h>

#include <xAODEventInfo/EventInfo.h>

#include <parquet/arrow/writer.h>

#include <arrow/filesystem/filesystem.h>

#include <iostream>

ContainerDumper :: ContainerDumper (const std::string& name,
				      ISvcLocator *pSvcLocator)
  : EL::AnaAlgorithm (name, pSvcLocator)
{
  declareProperty( "CollectionName", m_collectionName,
                   "Name of collection to dump." );
}

StatusCode ContainerDumper :: initialize ()
{
  //
  // Create output file

  // Determine where to save it by looking at the
  // worker's ROOT output.
  TFile *fh=wk()->getOutputFile( "PARQUET" );

  std::string path=fh->GetPath();
  path=path.substr(0, path.size()-7); // remote .root:/

  // Create the filesystem from output directory
  std::string internalpath;
  std::shared_ptr<arrow::fs::FileSystem> fs = arrow::fs::FileSystemFromUriOrPath(path, &internalpath).ValueOrDie();
  std::cout << "internalpath = " << internalpath << std::endl;
  fs->CreateDir(internalpath); // make sure the output directory exists

  // Create Container object
  std::shared_ptr<arrow::io::OutputStream> outfile;

  PARQUET_ASSIGN_OR_THROW(
                          outfile,
                          fs->OpenOutputStream(path+"/"+name()+".parquet"));

  //
  // File Schema
  parquet::schema::NodeVector fields;

  fields.push_back(parquet::schema::PrimitiveNode::Make("runNumber",
							parquet::Repetition::REQUIRED,
							parquet::Type::INT32,
							parquet::ConvertedType::UINT_32));

  fields.push_back(parquet::schema::PrimitiveNode::Make("eventNumber",
							parquet::Repetition::REQUIRED,
							parquet::Type::INT64,
							parquet::ConvertedType::INT_64));

  fields.push_back(parquet::schema::PrimitiveNode::Make("idx",
							parquet::Repetition::REQUIRED,
							parquet::Type::INT32,
							parquet::ConvertedType::UINT_32));

  const parquet::schema::NodeVector& colfields=this->fields();
  fields.insert(fields.end(), colfields.begin(), colfields.end());

  std::shared_ptr<parquet::schema::GroupNode> schema =
    std::static_pointer_cast<parquet::schema::GroupNode>(parquet::schema::GroupNode::Make("schema", parquet::Repetition::REQUIRED, fields));

  // Create writer
  parquet::WriterProperties::Builder writer_properties;
  //writer_properties.compression(parquet::Compression::BROTLI);

  os =std::make_unique<parquet::StreamWriter>(parquet::ParquetFileWriter::Open(outfile, schema, writer_properties.build()));

  // Start counter
  m_idx=0;

  return StatusCode::SUCCESS;
}

StatusCode ContainerDumper :: startrow ()
{
  const xAOD::EventInfo *eventInfo = nullptr;
  ANA_CHECK (evtStore()->retrieve (eventInfo, "EventInfo"));

  (*os) << eventInfo->runNumber() << (int64_t)eventInfo->eventNumber() << m_idx++;

  return StatusCode::SUCCESS;
}

StatusCode ContainerDumper :: endrow ()
{
  (*os) << parquet::EndRow;

  return StatusCode::SUCCESS;
}

StatusCode ContainerDumper :: endevent ()
{
  m_idx=0;

  return StatusCode::SUCCESS;
}

StatusCode ContainerDumper :: finalize ()
{
  return StatusCode::SUCCESS;
}
