#include <ParquetDumper/ParquetDumper.h>

#include <EventLoop/Worker.h>

#include <xAODEventInfo/EventInfo.h>

#include <parquet/arrow/writer.h>

#include <arrow/filesystem/filesystem.h>

#include <iostream>

ParquetDumper :: ParquetDumper (const std::string& name,
				ISvcLocator *pSvcLocator)
  : EL::AnaAlgorithm (name, pSvcLocator)
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  This is also where you
  // declare all properties for your algorithm.  Note that things like
  // resetting statistics variables or booking histograms should
  // rather go into the initialize() function.
}



StatusCode ParquetDumper :: initialize ()
{
  //
  // Create output file

  // Determine where to save it by looking at the
  // worker's ROOT output.
  TFile *fh=wk()->getOutputFile( "PARQUET" );

  std::string path=fh->GetPath();
  path=path.substr(0, path.size()-7); // remote .root:/

  std::cout << path << std::endl;

  // Create the filesystem from output directory
  std::string internalpath;
  std::shared_ptr<arrow::fs::FileSystem> fs = arrow::fs::FileSystemFromUriOrPath(path, &internalpath).ValueOrDie();
  std::cout << "internalpath = " << internalpath << std::endl;
  fs->CreateDir(internalpath); // make sure the output directory exists

  // Create Parquet object
  std::shared_ptr<arrow::io::OutputStream> outfile;

  PARQUET_ASSIGN_OR_THROW(
                          outfile,
                          fs->OpenOutputStream(path+"/EventInfo.parquet"));

  //
  // File Schema
  parquet::schema::NodeVector fields;

  fields.push_back(parquet::schema::PrimitiveNode::Make(
							"runNumber", parquet::Repetition::REQUIRED, parquet::Type::INT32,
							parquet::ConvertedType::UINT_32));

  fields.push_back(parquet::schema::PrimitiveNode::Make(
							"eventNumber", parquet::Repetition::REQUIRED, parquet::Type::INT64,
							parquet::ConvertedType::INT_64));
  
  //const std::vector<std::shared_ptr<arrow::Field>>& colfields=this->fields();
  //fields.insert(fields.end(), colfields.begin(), colfields.end());

  std::shared_ptr<parquet::schema::GroupNode> schema =
    std::static_pointer_cast<parquet::schema::GroupNode>(parquet::schema::GroupNode::Make("schema", parquet::Repetition::REQUIRED, fields));

  // Create writer
  parquet::WriterProperties::Builder writer_properties;
  //writer_properties.compression(parquet::Compression::BROTLI);

  os =std::make_unique<parquet::StreamWriter>(parquet::ParquetFileWriter::Open(outfile, schema, writer_properties.build()));

  return StatusCode::SUCCESS;
}

StatusCode ParquetDumper :: execute ()
{
  const xAOD::EventInfo *eventInfo = nullptr;
  ANA_CHECK (evtStore()->retrieve (eventInfo, "EventInfo"));

  std::cout << "Run over " << eventInfo->runNumber() << std::endl;

  (*os) << eventInfo->runNumber() << (int64_t)eventInfo->eventNumber() << parquet::EndRow;

  return StatusCode::SUCCESS;
}



StatusCode ParquetDumper :: finalize ()
{
  return StatusCode::SUCCESS;
}
