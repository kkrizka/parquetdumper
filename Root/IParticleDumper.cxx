#include <ParquetDumper/IParticleDumper.h>

#include <xAODBase/IParticleContainer.h>

IParticleDumper :: IParticleDumper (const std::string& name,
				    ISvcLocator *pSvcLocator)
  : ContainerDumper (name, pSvcLocator)
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  This is also where you
  // declare all properties for your algorithm.  Note that things like
  // resetting statistics variables or booking histograms should
  // rather go into the initialize() function.
}

parquet::schema::NodeVector IParticleDumper::fields()
{ 
  parquet::schema::NodeVector fields;

  fields.push_back(parquet::schema::PrimitiveNode::Make("pt",
							parquet::Repetition::REQUIRED,
							parquet::Type::FLOAT,
							parquet::ConvertedType::NONE));

  fields.push_back(parquet::schema::PrimitiveNode::Make("eta",
							parquet::Repetition::REQUIRED,
							parquet::Type::FLOAT,
							parquet::ConvertedType::NONE));

  fields.push_back(parquet::schema::PrimitiveNode::Make("phi",
							parquet::Repetition::REQUIRED,
							parquet::Type::FLOAT,
							parquet::ConvertedType::NONE));

  fields.push_back(parquet::schema::PrimitiveNode::Make("m",
							parquet::Repetition::REQUIRED,
							parquet::Type::FLOAT,
							parquet::ConvertedType::NONE));

  return fields;
}

StatusCode IParticleDumper :: execute ()
{
  const xAOD::IParticleContainer* iparticles = nullptr;
  ANA_CHECK (evtStore()->retrieve ( iparticles, m_collectionName )); 
  for (const xAOD::IParticle* iparticle : *iparticles) {
    ANA_CHECK (startrow());

    ANA_CHECK (fill(iparticle));

    ANA_CHECK (endrow());
  }
  ANA_CHECK (endevent());

  return StatusCode::SUCCESS;
}

StatusCode IParticleDumper :: fill(const xAOD::IParticle* iparticle)
{
  (*os) << (float)iparticle->pt();
  (*os) << (float)iparticle->eta();
  (*os) << (float)iparticle->phi();
  (*os) << (float)iparticle->m();

  return StatusCode::SUCCESS;
}
