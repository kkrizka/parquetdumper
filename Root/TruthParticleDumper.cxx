#include <ParquetDumper/TruthParticleDumper.h>

#include <xAODTruth/TruthParticleContainer.h>

TruthParticleDumper :: TruthParticleDumper (const std::string& name,
					    ISvcLocator *pSvcLocator)
  : IParticleDumper (name, pSvcLocator)
{ }

parquet::schema::NodeVector TruthParticleDumper::fields()
{ 
  parquet::schema::NodeVector fields;

  // Parent's fields
  const parquet::schema::NodeVector &pfields = IParticleDumper::fields();
  fields.insert(fields.end(), pfields.begin(), pfields.end());

  // My field's
  fields.push_back(parquet::schema::PrimitiveNode::Make("pdg",
							parquet::Repetition::REQUIRED,
							parquet::Type::INT32,
							parquet::ConvertedType::INT_32));

  fields.push_back(parquet::schema::PrimitiveNode::Make("status",
							parquet::Repetition::REQUIRED,
							parquet::Type::INT32,
							parquet::ConvertedType::INT_32));

  return fields;
}

StatusCode TruthParticleDumper :: execute ()
{
  const xAOD::TruthParticleContainer* truthparticles = nullptr;
  ANA_CHECK (evtStore()->retrieve ( truthparticles, m_collectionName ));
  for (const xAOD::TruthParticle* truthparticle : *truthparticles) {
    ANA_CHECK (startrow());

    ANA_CHECK (IParticleDumper::fill(truthparticle));

    (*os) << truthparticle->pdgId();
    (*os) << truthparticle->status();
    
    ANA_CHECK (endrow());
  }
  ANA_CHECK (endevent());

  return StatusCode::SUCCESS;
}
