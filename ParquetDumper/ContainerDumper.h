#pragma once

#include <AnaAlgorithm/AnaAlgorithm.h>

#include <arrow/api.h>

#include <parquet/arrow/writer.h>
#include <parquet/stream_writer.h>

class ContainerDumper : public EL::AnaAlgorithm
{
public:
  // this is a standard algorithm constructor
  ContainerDumper (const std::string& name, ISvcLocator* pSvcLocator);

  // these are the functions inherited from Algorithm
  virtual StatusCode initialize () override;
  //virtual StatusCode execute () override;
  virtual StatusCode finalize () override;

public: // PROPERTIES
  //! Name of collection to dump
  std::string m_collectionName;

protected:
  virtual parquet::schema::NodeVector fields() =0;

  StatusCode startrow();
  StatusCode endrow();
  StatusCode endevent();

  //! Output stream
  std::unique_ptr<parquet::StreamWriter> os;

private:
  //! Container index counter
  uint32_t m_idx;
};
