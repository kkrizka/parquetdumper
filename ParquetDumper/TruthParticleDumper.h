#pragma once

#include "ParquetDumper/IParticleDumper.h"

class TruthParticleDumper : public IParticleDumper
{
public:
  // this is a standard algorithm constructor
  TruthParticleDumper (const std::string& name, ISvcLocator* pSvcLocator);

  // these are the functions inherited from Algorithm
  virtual StatusCode execute () override;

protected:
  virtual parquet::schema::NodeVector fields() override;
};
