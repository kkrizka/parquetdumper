#ifndef PARQUETDUMPER_PARQUETDUMPERDICT_H
#define PARQUETDUMPER_PARQUETDUMPERDICT_H

// This file includes all the header files that you need to create
// dictionaries for.

#include <ParquetDumper/ParquetDumper.h>
#include <ParquetDumper/ContainerDumper.h>
#include <ParquetDumper/IParticleDumper.h>
#include <ParquetDumper/TruthParticleDumper.h>

#endif // PARQUETDUMPER_PARQUETDUMPERDICT_H
