#pragma once

#include <xAODBase/IParticle.h>

#include "ParquetDumper/ContainerDumper.h"

class IParticleDumper : public ContainerDumper
{
public:
  // this is a standard algorithm constructor
  IParticleDumper (const std::string& name, ISvcLocator* pSvcLocator);

  // these are the functions inherited from Algorithm
  virtual StatusCode execute () override;

protected:
  virtual parquet::schema::NodeVector fields() override;

  StatusCode fill(const xAOD::IParticle* iparticle);
};
