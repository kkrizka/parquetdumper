#pragma once

#include <AnaAlgorithm/AnaAlgorithm.h>

#include <arrow/api.h>

#include <parquet/arrow/writer.h>
#include <parquet/stream_writer.h>

class ParquetDumper : public EL::AnaAlgorithm
{
public:
  // this is a standard algorithm constructor
  ParquetDumper (const std::string& name, ISvcLocator* pSvcLocator);

  // these are the functions inherited from Algorithm
  virtual StatusCode initialize () override;
  virtual StatusCode execute () override;
  virtual StatusCode finalize () override;

protected:
  void write();

private:
  // Configuration, and any other types of variables go here.
  //float m_cutValue;
  //TTree *m_myTree;
  //TH1 *m_myHist;

  std::unique_ptr<parquet::StreamWriter> os; // (parquet::ParquetFileWriter::Open(outfile, schema, writer_properties.build()));
  
  //! Number of rows per RowGroup
  uint32_t m_numrowsingroup;

  //! Unflushed number of rows
  uint32_t m_rows;
  
  //! Builders
  arrow::Int32Builder b_eventNumber;
  arrow::Int32Builder b_runNumber;

};
